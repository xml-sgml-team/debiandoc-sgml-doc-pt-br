## ----------------------------------------------------------------------
## Makefile : makefile for debiandoc-sgml-doc
## ----------------------------------------------------------------------

## ----------------------------------------------------------------------
## Document definitions
doc_lang	:= pt_BR
doc_name	:= debiandoc-sgml
doc_sgml	:= $(doc_name).$(doc_lang).sgml
doc_pdf		:= $(doc_name).$(doc_lang).pdf
doc_txt		:= $(doc_name).$(doc_lang).txt
doc_html	:= $(doc_name).html

## ----------------------------------------------------------------------
## Package definitions
pkg_lang	:= pt-br

## ----------------------------------------------------------------------
## General definitions
LN		:= /bin/ln -s
RMR		:= /bin/rm -rf

## ----------------------------------------------------------------------
## Targets
all:		$(doc_sgml)

		onsgmls -wall -E20 -gues $^
		debiandoc2html -l $(doc_lang) -b $(doc_name) -c $^
		$(LN) index.$(pkg_lang).html $(doc_html)/index.html
		debiandoc2text -l $(doc_lang) $^

clean:

		$(RMR) $(doc_html) $(doc_txt)

## ----------------------------------------------------------------------
